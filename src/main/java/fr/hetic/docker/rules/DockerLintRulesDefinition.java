package fr.hetic.docker.rules;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.api.server.rule.RulesDefinitionXmlLoader;

import fr.hetic.docker.languages.DockerLanguage;

public class DockerLintRulesDefinition implements RulesDefinition {

	protected static final String KEY = "dockerlint";
	protected static final String NAME = "DockerLint";

	public static final String REPO_KEY = DockerLanguage.KEY + "-" + KEY;
	public static final String REPO_NAME = DockerLanguage.NAME + "-" + NAME;

	private void defineRulesForLanguage(Context context, String repositoryKey, String repositoryName,
			String languageKey) {
		NewRepository repository = context.createRepository(repositoryKey, languageKey).setName(repositoryName);

		InputStream rulesXml = this.getClass().getResourceAsStream("/docker-rules.xml");
		if (rulesXml != null) {
			RulesDefinitionXmlLoader rulesLoader = new RulesDefinitionXmlLoader();
			rulesLoader.load(repository, rulesXml, StandardCharsets.UTF_8.name());
		}

		repository.done();
	}

	@Override
	public void define(Context context) {
		defineRulesForLanguage(context, REPO_KEY, REPO_NAME, DockerLanguage.KEY);
	}

}
