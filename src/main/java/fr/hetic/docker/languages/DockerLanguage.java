package fr.hetic.docker.languages;

import org.sonar.api.config.Configuration;
import org.sonar.api.resources.AbstractLanguage;

public class DockerLanguage extends AbstractLanguage {

	public static final String NAME = "Docker";

	public static final String KEY = "docker";
	
	private final Configuration configuration;
	
	public DockerLanguage(Configuration configuration) {
		super(KEY, NAME);
		this.configuration = configuration;
	}
	
	@Override
	public String[] getFileSuffixes() {
		// TODO Auto-generated method stub
		return this.configuration.getStringArray("sonar.lang.patterns.docker");
	}

}
